a = 42
i = 0
while True:
	try:
		i += 1
		x = input()
		a += 1 / int(x)
	except ValueError:
		print('Строчка', i,'содержит не целое число! (в рассчет оно не бралось)')
		continue
	except EOFError:
		break
	except ZeroDivisionError:
		print('Строчка', i,'содержит ноль! (в рассчет он не брался)')
		continue
print('Сумма ряда в итоге: ', a)
