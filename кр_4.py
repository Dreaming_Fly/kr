from random import randint, uniform
from timeit import default_timer
import matplotlib.pyplot as plt
import matplotlib.ticker as tcr


# функция перемножения матриц
def matrix(mat1, mat2):
    result = [[sum(a * b for a, b in zip(i, j))
               for j in zip(*mat2)] for i in mat1]
    return result


# ввод данных массивов
elem, time_int, time_flt = [], [], []

# создание матриц
for i in range(10, 101, 10):
    mat_int1 = [[randint(1, 10) for i in range(i)] for j in range(i)]
    mat_int2 = [[randint(1, 10) for i in range(i)] for j in range(i)]
    mat_flt1 = [[uniform(1, 10) for i in range(i)] for j in range(i)]
    mat_flt2 = [[uniform(1, 10) for i in range(i)] for j in range(i)]

    # ввод переменных
    avg_int, avg_flt = 0, 0

    for _ in range(3):
        # время перемножения матриц
        t0 = default_timer()
        n1 = matrix(mat_int1, mat_int2)
        avg_int += default_timer() - t0

        t0 = default_timer()
        n2 = matrix(mat_flt1, mat_flt2)
        avg_flt += default_timer() - t0

    # заносим все данные в массивы
    elem.append(i ** 2)
    time_int.append(avg_int / 3)
    time_flt.append(avg_flt / 3)

fig, ax = plt.subplots()
plt.plot(elem, time_int, marker='.', label='int')
plt.plot(elem, time_flt, marker='.', label='float')
plt.title('График зависимости времени подсчета от размерности матрицы')
ax.set_xlabel('Размер матрицы')
ax.set_ylabel('Время')
ax.set_xlim([100, i ** 2])
ax.xaxis.set_major_locator(tcr.MultipleLocator(1000))
plt.grid()
plt.savefig('picture.png')
