from copy import deepcopy
from random import randint
from timeit import default_timer


# Функция нахождения определителя матрицы
def determinant(matr, n):
    if n == 1:
        return matr[0][0]
    elif n == 2:
        return matr[0][0] * matr[1][1] - matr[1][0] * matr[0][1]
    else:
        s = 0
        for i in range(n):
            matr_1 = deepcopy(matr)
            matr_1.pop(0)
            for j in range(n - 1):
                matr_1[j].pop(i)
            s += (-1) ** i * matr[0][i] * determinant(matr_1, n - 1)
        return s


# Функция для решения системы уравнений методом Крамера
def method_cramer(coefs, free_coefs, num, d):
    value = []
    for i in range(num):  # Для каждой переменной
        line = deepcopy(coefs)
        for j in range(num):
            # Заменяем столбец с коэффициентами при переменной на свободные
            line[j][i] = free_coefs[j]
        # Находим определитель матрицы с заменёнными коэффициентами
        line_det = determinant(line, num)
        value.append(line_det / d)
    return value


# Функция для решения системы уравнений методом Гаусса
def method_gauss(matr, num):
    for i in range(0, num):
        maximum_element = abs(matr[i][i])
        maximum_range = i
        for j in range(i + 1, num):
            if abs(matr[j][i]) > maximum_element:
                maximum_element = abs(matr[j][i])
                maximum_range = j
        for j in range(i, num + 1):
            matr[maximum_range][j], matr[i][j] = matr[i][j], matr[maximum_range][j]
        for j in range(i + 1, num):
            result = - (matr[j][i] / matr[i][i])
            for k in range(i, num + 1):
                if i == k:
                    matr[j][k] = 0
                else:
                    matr[j][k] += result * matr[i][k]

    solution = [0 for elem in range(num)]
    for i in range(num - 1, -1, -1):
        solution[i] = matr[i][num] / matr[i][i]
        for j in range(i - 1, -1, -1):
            matr[j][num] -= matr[j][i] * solution[i]
    return solution


# Функция записи в файл
def func(time_kramer, time_gauss):
    with open('time_all.txt', 'w') as file:
        file.write('Метод Крамера\n')
        for element in time_kramer:
            file.write(str(element).replace('.', ',') + '\n')
        file.write('Метод Гаусса\n')
        for element in time_gauss:
            file.write(str(element).replace('.', ',') + '\n')
        file.close()


# Создание массивов для хранения данных
coefficients, free_coefficients = [], []
end_time1, end_time2 = [], []
# Кол-во уравнений
min_numbers = int(input('Мин кол-во уравнений '))
max_numbers = int(input('Макс кол-во уравнений '))
for elem in range(min_numbers, max_numbers + 1):
    for i in range(elem):
        coefficients.append([])
        for j in range(elem):
            # Записываем в массив коэффициенты
            coefficients[i].append(randint(-10, 10))
        # Записываем в массив свободные коэффициенты
        free_coefficients.append(randint(-10, 10))

    # Определитель матрицы
    det = determinant(coefficients, elem)

    matrix = [[randint(0, 9) for i in range(elem + 1)] for j in range(elem)]
    start_time = default_timer()
    result_gauss = method_gauss(matrix, elem)
    end_time2.append(default_timer() - start_time)

    if det == 0:
        print("Определитель равен нулю")
    else:
        start_time = default_timer()
        result_cramer = method_cramer(coefficients, free_coefficients, elem, det)
        end_time1.append(default_timer() - start_time)
func(end_time1, end_time2)
