from math import sqrt
from random import randint, uniform
from timeit import default_timer
import matplotlib.pyplot as plt


# Создание вектора z и определение его длины
def creat_vector(v_x, v_y, c):
    num = [vi + wi for vi, wi in zip(v_x, v_y)]
    z = [c * vi for vi in num]
    return sqrt(sum(i ** 2 for i in z))


# Создаем векторы
v_int, v_flt = [], []
x_int = [randint(1, 100) for i in range(5)]
y_int = [randint(1, 100) for j in range(5)]
x_flt = [uniform(1, 100) for k in range(5)]
y_flt = [uniform(1, 100) for n in range(5)]

# Константа
const = 10
tell_int, tell_flt = 0, 0
time_int, time_flt = [], []
for i in range(1, 11):
    # Высчитывание время, затраченное на создание вектора z(int)
    t0 = default_timer()
    v_int.append(creat_vector(x_int, y_int, const))
    tell_int += (default_timer() - t0)
    time_int.append(tell_int)

    # Высчитывание время, затраченное на создание вектора z(float)
    t0 = default_timer()
    v_flt.append(creat_vector(x_flt, y_flt, const))
    tell_flt += (default_timer() - t0)
    time_flt.append(tell_flt)

    # Увеличиваем длину векторов путем увеличения одной координаты
    y_int[0] += 20
    y_flt[0] += 20

fig, ax = plt.subplots()
plt.plot(v_int, time_int, marker='.', label='int')
plt.plot(v_flt, time_flt, marker='.', label='float')
plt.title('График зависимости времени подсчета от длины вектора')
ax.set_xlabel('Длина')
ax.set_ylabel('Время')
plt.grid()
plt.savefig('picture1.png')
